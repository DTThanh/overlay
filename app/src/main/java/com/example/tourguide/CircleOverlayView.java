package com.example.tourguide;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

public class CircleOverlayView extends LinearLayout {
    private Bitmap bitmap;
    private int count;

    public CircleOverlayView(Context context) {
        super(context);
    }

    public CircleOverlayView(Context context, int index) {
        super(context);
        this.count = index;
    }

    public CircleOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleOverlayView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (bitmap == null) {
            createWindowFrame();
            canvas.drawBitmap(bitmap, 0, 0, null);
        }

    }


    protected void createWindowFrame() {
        bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas osCanvas = new Canvas(bitmap);

        RectF outerRectangle = new RectF(0, 0, getWidth(), getHeight());

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(getResources().getColor(R.color.m_p_background_metfoneplus_guide_line));
        paint.setAlpha(200);
        osCanvas.drawRect(outerRectangle, paint);

        paint.setColor(Color.TRANSPARENT);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        float centerX = getWidth() / 2;
        float centerY = getHeight() / 2;
//        float radius = getResources().getDimensionPixelSize(R.dimen.radius);
        //draw bigger circle
        osCanvas.drawCircle(centerX, (float) (centerY * 0.72), (float) (centerX * 0.9), paint);
        Log.d("AAAAA", count + "");
        //draw smaller circle
        osCanvas.drawCircle(centerX, (float) (centerY * 1.2), (float) (centerX * 0.43), paint);

    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        bitmap = null;
    }
}

