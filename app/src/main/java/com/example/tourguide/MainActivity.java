package com.example.tourguide;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class MainActivity extends AppCompatActivity {
    private Paint paint;
    private Bitmap bitmap;
    private Canvas canvas;
    private float centerX, centerY;
    private ImageView imageView;
    private ConstraintLayout constraintLayout;
    private int i;
    private CircleOverlayView circleOverlayView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}